#ifndef TIMER_H
#define TIMER_H
#include <iostream>
#include <chrono>

using namespace std::chrono;
using namespace std;
class Timer
{
public:
    Timer();
    virtual ~Timer();
    //method desired to measure time of execution other method
    template <typename T, typename R, typename ...Args>
    static int getExecutionTimeInMilliseconds(T & object, R (T::*mf)(Args...), Args &&... args)
    {
        time_point<high_resolution_clock> start = high_resolution_clock::now();
        (object.*mf)(forward<Args>(args)...);
        time_point<high_resolution_clock> end = high_resolution_clock::now();
        int time = duration_cast<milliseconds>(end - start).count();
        cout<<"Function is proxed, time "<<time<<endl;
        return time;
    }

protected:
private:
};

#endif // TIMER_H
