#ifndef FASTACCESS_H
#define FASTACCESS_H
#include "WorkingImage.h"

class FastAccess
{
public:
    template< typename T, int N>
    static const inline T get(WorkingImage<T, N>* work,int height, int width, int color)
    {
        return work->helper[height][width*N+color];
    }
    template< typename T, int N>
    static const inline T getR(WorkingImage<T, N>* work,int height, int width)
    {
        return work->helper[height][width*N+redCode];
    }
    template< typename T, int N>
    static const inline T getG1(WorkingImage<T, N>* work,int height, int width)
    {
        return work->helper[height][width*N+green1Code];
    }
    template< typename T, int N>
    static const inline T getB(WorkingImage<T, N>* work,int height, int width)
    {
        return work->helper[height][width*N+blueCode];
    }
    template< typename T, int N>
    static const inline T getG2(WorkingImage<T, N>* work,int height, int width)
    {
        return work->helper[height][width*N+green2Code];
    }
    FastAccess() {}
    virtual ~FastAccess() {}
protected:
private:
};

#endif // FASTACCESS_H
