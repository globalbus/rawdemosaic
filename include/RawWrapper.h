#ifndef RAWWRAPPER_H
#define RAWWRAPPER_H
#include <iostream>
struct offset
{
    int x;
    int y;
    int code;
};
typedef unsigned short ushort;
typedef unsigned int uint;

class RawWrapper
{
public:
    RawWrapper();
    RawWrapper(RawWrapper& source);
    //virtual void cameraWhiteBalance()  = 0;
    virtual ~RawWrapper();
    virtual RawWrapper* copy()= 0;
    virtual int maxValue()=0;
    uint extend;
    uint height, width;
    virtual void cropToActiveArea()  = 0;
    virtual void doWhiteBalance()  = 0;
    bool isCropped=false;
    ushort * extendedRawImage=nullptr;
    ushort ** extendRawArray=nullptr;
    virtual int open(std::string * file)  = 0;
    //white balance multipliers
    float * multipliers=nullptr;
    virtual void decodeOffsets(int i)   = 0;
    //key: color code @see define rules for color codes
    //values: offset in bayer filter
    offset colorOffsets[4];
    //key:order in bayer filter
    //value:color code @see define rules for color codes
    int bayerOffsets[4];
    int blackLevels[4];
    void free();
    virtual double** getColorArray()=0;
protected:
    void setExtendValue();
    ushort * extendBayerArea(ushort* data);
    ushort * extendBayerArea(ushort* data, int extendBlocks);
    void buildArray();
    const double noConv[3][3] =
    {
        { 1.0, 0, 0},
        { 0, 1.0, 0 },
        { 0, 0, 1.0 }
    };
private:

};

#endif // RAWWRAPPER_H
