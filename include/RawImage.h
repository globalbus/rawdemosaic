#ifndef RAWIMAGE_H
#define RAWIMAGE_H

#include "WorkingImage.h"
#include "RawWrapper.h"
#include "PngReaderWriter.h"
#include "Settings.h"



typedef unsigned int uint;

class RAWImage
{
public:
    RAWImage(RawWrapper * sourceWrapper);
    ~RAWImage();
    void demosaic();
    void cameraWhiteBalance();
    void open(std::string * file);
    void copyForProcess();
    void savePNG(std::string * file);
    void colorCorrection();
    void applyPrefilter();
    void applyPostfilter();

    void nearestNeighborDemosaic_fast();
    void impulseFilter(int maskSize);
    void impulseFilterCombined(int maskSize);
    void impulseFilterPre(int maskSize);
    void bilinearDemosaic_fast();
    void simpleGradientsDemosaic();
    void VNGDemosaic();
    void medianFilter(int maskSize);
    void medianFilterPre(int maskSize);
    //void save(char* file);
    void impulseFilterBilinear(int maskSize);
    //getters/setters
    int getPrecision();

    //void copyRawDataToImage();

    WorkingImage<arrayType, 3> * image=nullptr;
protected:

    //1 dimension array that holds real data
    arrayType* singleMemoryBlock=nullptr;
    //key: color code @see define rules for color codes
    //values: offset in bayer filter
    offset * colorOffsets;
    //key:order in bayer filter
    //value:color code @see define rules for color codes
    int * bayerOffsets;
    //real raw data width and height
    uint height, width;
    uint imageSize=0;

    //value of bayer array extended edges
    int extend;

    bool blocked=true;
    ushort * extendedRawImage=nullptr;
    ushort ** extendRawArray=nullptr;
    void spreadBits();
    //formula to find color code on rawArray for given j - height, i - width
inline int bayerFind(int j, int i){
    return bayerOffsets[(i&1)|(j&1)<<1];
}
    //defaults
    int precision=16;
    RawWrapper * sourceWrapper = nullptr;
private:
    //call if you want to free buffers
    void free();

};

#endif // RAWIMAGE_H
