#ifndef RAWIMAGEOLD_H
#define RAWIMAGEOLD_H
#include "RawImage.h"

class RAWImageOld : public RAWImage
{
    public:
        RAWImageOld(RawWrapper * sourceWrapper);
        virtual ~RAWImageOld();
        void open(std::string * file);
        void medianFilter(int maskSize);
        void bilinearDemosaic();
        void nearestNeighborDemosaic();
        void copyForProcess();
        void savePNG(std::string * file);
    protected:
    private:
            //3 dimension array for easy access to pixels
            arrayType*** colors=nullptr;
};

#endif // RAWIMAGEOLD_H
