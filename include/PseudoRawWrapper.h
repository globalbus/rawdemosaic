#ifndef PSEUDORAWWRAPPER_H
#define PSEUDORAWWRAPPER_H

#include "RawWrapper.h"
#include "PngReaderWriter.h"
#include "WorkingImage.h"
#include "Settings.h"
class PseudoRawWrapper : public RawWrapper
{
    public:
        PseudoRawWrapper();
        virtual ~PseudoRawWrapper();
        PseudoRawWrapper(PseudoRawWrapper& source);
        RawWrapper* copy();
        int open(std::string * file);
        void decodeOffsets(int i);
        void cropToActiveArea();
        void doWhiteBalance();
        int maxValue();
        WorkingImage<arrayType, 3> * originalImage=nullptr;
        void free();
        double ** getColorArray();
        int maxValueWithWhiteBalance(){return maxValue();}
    protected:
    private:
};

#endif // PSEUDORAWWRAPPER_H
