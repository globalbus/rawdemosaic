#ifndef BILINEAROLDSCENARIO_H
#define BILINEAROLDSCENARIO_H
#include "Scenario.h"
#include "RAWImageOld.h"
class BilinearOldScenario : public Scenario
{
    public:
        BilinearOldScenario(RawWrapper * rawWrapper);
        virtual ~BilinearOldScenario();
        void launchScenario(string * file);
    protected:
        RAWImageOld * RAWEngine= nullptr;
    private:
};

#endif // BILINEAROLDSCENARIO_H
