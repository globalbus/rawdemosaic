#ifndef SCENARIO_H
#define SCENARIO_H
#include "RawImage.h"
#include "RawWrapper.h"
#include "Timer.h"
#include <iostream>
class Scenario
{
public:
    class Times
    {
    public:
        Times();
        template<typename T>
        void writeTo(T text){
            csvFile<<text;
            csvFile<<delimiter;
        }
        virtual ~Times();
        void next();
    private:
        ofstream csvFile;
        const char delimiter =',';
        const char * firstColumn = "filename,algorithmName,openTime,preprocessingTime,demosaicingTime,postProcessingTime,savingTime,\n";

    };

    Scenario(RawWrapper * wrapper);
    virtual ~Scenario();
    virtual void launchScenario(string* file);
    WorkingImage<arrayType,3> * getDemosaicedImage();


    static Times& getInstance()
    {
        static Times instance;
        return instance;
    }
    string algorithmName;

protected:
    RAWImage * RAWEngine = nullptr;

private:

};

#endif // SCENARIO_H
