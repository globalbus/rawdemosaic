#ifndef LIBRAWWRAPPER_H
#define LIBRAWWRAPPER_H
#include <libraw/libraw.h>
#include "RawWrapper.h"
#include "Settings.h"


class LibRawWrapper : public RawWrapper
{

public:
    LibRawWrapper();
    LibRawWrapper(LibRawWrapper& source);
    RawWrapper* copy();
    int maxValue();
    virtual ~LibRawWrapper();
    int open(std::string * file);
    void decodeOffsets(int i);
    void cropToActiveArea();
    void doWhiteBalance();
    double** getColorArray();

protected:
private:
    LibRaw iProcessor;
    void free();
//    const double xyz_rgb[3][3] =  			/* XYZ from RGB */
//    {
//        { 0.412453, 0.357580, 0.180423 },
//        { 0.212671, 0.715160, 0.072169 },
//        { 0.019334, 0.119193, 0.950227 }
//    };

};

#endif // LIBRAWWRAPPER_H
