#ifndef WORKINGIMAGE_H
#define WORKINGIMAGE_H
#include <iostream>
#include <cstring>
#include <fstream>
#include <climits>


#define redCode 0
#define green1Code 1
#define blueCode 2
#define green2Code 3

#define index1 height
#define index2 width
#define index3 color

typedef unsigned short ushort;
typedef ushort arrayType;
template<typename T, int N>
class WorkingImage
{
public:
    WorkingImage(int width, int height)
    {
        this->height=height;
        this->width=width;
        extendedWidth=width;
        singleMemoryBlock = new T[index1*index2*index3];
        buildArray();
    }
    WorkingImage(T * data, int width, int height)
    {
        //color=3;
        this->height=height;
        this->width=width;
        extendedWidth=width;
        singleMemoryBlock=data;
        buildArray();
    }
    template< typename NewTypeT, int NewN>
    WorkingImage(WorkingImage<NewTypeT,NewN> *image)
    {
        this->height=image->height;
        this->width=image->width;
        this->extend=image->extend;
        this->extendedWidth=(image->extend<<1)+image->width;
        int size=(width+(extend<<1))*(height+(extend<<1))*color;
        this->singleMemoryBlock= new T[size];
        for(int i=0; i<size; i++)
            this->singleMemoryBlock[i]=image->singleMemoryBlock[i];
        buildArray();
    }
    WorkingImage<T, N>* extendWorkingImage(int extend)
    {
        WorkingImage<T, N>* newImage = new WorkingImage<T, N>();
        newImage->height=height;
        newImage->width=width;
        newImage->extend=extend;
        newImage->extendedWidth=(extend<<1)+width;
        T* newSingleMemoryBlock;
        if(extend==1)
            newSingleMemoryBlock=extendArea();
        else
            newSingleMemoryBlock=extendArea(extend);
        newImage->singleMemoryBlock=newSingleMemoryBlock;
        newImage->buildArray();
        return newImage;
    }

    const T get(int height, int width, int color)
    {
        return helper[height][width*this->color+color];
    }
    void set(int height, int width, int color, T value)
    {
        helper[height][width*this->color+color]=value;

    }
    void buildArray()
    {
        helper= new T*[height+(extend<<1)];
        for(int j=0, l=extend*color; j<height+(extend<<1); j++)
        {
            helper[j] = &singleMemoryBlock[l];
            l+=(extendedWidth)*color;
        }
        helper=helper+extend;
    }
    void checkedSet(int height, int width, int color, int value)
    {
        if(value<0)
            value=0;
        else if(value>maxAmount)
            value=maxAmount;
        helper[height][width*this->color+color]=value;
    }
    const T getR(int height, int width)
    {
        return helper[height][width*this->color+redCode];
    }
    const T getG1(int height, int width)
    {
        return helper[height][width*this->color+green1Code];
    }
    const T getB(int height, int width)
    {
        return helper[height][width*this->color+blueCode];
    }

    const T getG2(int height, int width)
    {
        return helper[height][width*this->color+green2Code];
    }
    void setR(int height, int width,T value)
    {
        helper[height][width*this->color+redCode]=value;
    }
    void setG1(int height, int width, T value)
    {
        helper[height][width*this->color+green1Code]=value;
    }
    void setB(int height, int width, T value)
    {
        helper[height][width*this->color+blueCode]=value;
    }
    void setG2(int height, int width, T value)
    {
        helper[height][width*this->color+green2Code]=value;
    }

    void checkedSetR(int height, int width,int value)
    {
        if(value<0)
            value=0;
        else if(value>maxAmount)
            value=maxAmount;
        helper[height][width*this->color+redCode]=value;
    }
    void checkedSetG1(int height, int width, int value)
    {
        if(value<0)
            value=0;
        else if(value>maxAmount)
            value=maxAmount;
        helper[height][width*this->color+green1Code]=value;
    }
    void checkedSetB(int height, int width, int value)
    {
        if(value<0)
            value=0;
        else if(value>maxAmount)
            value=maxAmount;
        helper[height][width*this->color+blueCode]=value;
    }
    void checkedSetG2(int height, int width, int value)
    {
        if(value<0)
            value=0;
        else if(value>maxAmount)
            value=maxAmount;
        helper[height][width*this->color+green2Code]=value;
    }

    ~WorkingImage()
    {
        delete[] singleMemoryBlock;
        delete[] (helper-extend);
    }
    T* singleMemoryBlock=nullptr;
    T** helper=nullptr;
    int width, height;
    int extend=0;

protected:

private:
    int extendedWidth;
    T * extendArea(int extendBlocks)
    {
        T * newArray = new T[(height+(extendBlocks<<1))*(width+(extendBlocks<<1))*color];
        int blockWidth=width*color;
        //copy first rows
        T * src= singleMemoryBlock+blockWidth*(extendBlocks-1);
        T * dest=newArray;
        for(int j=0; j<extendBlocks; j++)
        {
            src+=(extendBlocks-1)*color;//0
            for(int i=0; i<extendBlocks; i++)
            {
                memcpy(dest, src, color*sizeof(T));
                dest+=color;
                src-=color;//-1
            }
            src+=color;//0

            memcpy(dest, src, blockWidth*sizeof(T));
            dest+=blockWidth;
            src+=blockWidth;//width

            src-=color;//width-1
            for(int i=0; i<extendBlocks; i++)
            {
                memcpy(dest, src, color*sizeof(T));
                dest+=color;
                src-=color;//width-2
            }
            src+=(extendBlocks+1)*color;

            src-=blockWidth<<1;
        }
        src+=blockWidth;
#ifdef DEBUG
        if(src!=singleMemoryBlock)
            std::cout<<"error1 "<<singleMemoryBlock<<" "<<src<<std::endl;
#endif
        //copy center
        for(int j=0; j<height; j++)
        {
            src+=(extendBlocks-1)*color;
            for(int i=0; i<extendBlocks; i++)
            {
                memcpy(dest, src, color*sizeof(T));
                dest+=color;
                src-=color;
            }
            src+=color;
#ifdef DEBUG
            if(src!=singleMemoryBlock+j*width*color)
                std::cout<<"error2 "<<singleMemoryBlock<<" "<<src<<std::endl;
#endif
            memcpy(dest, src, blockWidth*sizeof(T));
            dest+=blockWidth;
            src+=blockWidth;
            src-=color;
            for(int i=0; i<extendBlocks; i++)
            {
                memcpy(dest, src, color*sizeof(T));
                dest+=color;
                src-=color;
            }
            src+=(extendBlocks+1)*color;
        }
#ifdef DEBUG
        if(src!=singleMemoryBlock+height*width*color)
            std::cout<<"error2 "<<singleMemoryBlock<<" "<<src<<std::endl;
#endif

        //copy last rows
        src-=blockWidth;
        for(int j=0; j<extendBlocks; j++)
        {
            src+=(extendBlocks-1)*color;
            for(int i=0; i<extendBlocks; i++)
            {
                memcpy(dest, src, color*sizeof(T));
                dest+=color;
                src-=color;
            }
            src+=color;

            memcpy(dest, src, blockWidth*sizeof(T));
            dest+=blockWidth;
            src+=blockWidth;
            src-=color;
            for(int i=0; i<extendBlocks; i++)
            {
                memcpy(dest, src, color*sizeof(T));
                dest+=color;
                src-=color;
            }
            src+=(extendBlocks+1)*color;
            src-=blockWidth<<1;
        }
        src+=blockWidth*(extendBlocks+1);
#ifdef DEBUG
        if(src!=singleMemoryBlock+height*width*color)
            std::cout<<"error3 "<<singleMemoryBlock<<" "<<src<<std::endl;
#endif
        return newArray;
    }
    T * extendArea()
    {
        T * newArray = new T[(height+2)*(width+2)*color];
        int blockWidth=width*color;
        //copy first rows
        T * src= singleMemoryBlock;
        T * dest=newArray;

        memcpy(dest, src, color*sizeof(T));
        dest+=color;

        memcpy(dest, src, blockWidth*sizeof(T));
        dest+=blockWidth;
        src+=blockWidth;//width

        src-=color;//width-1
        memcpy(dest, src, color*sizeof(T));
        dest+=color;
        src+=color;

        src-=blockWidth;
#ifdef DEBUG
        if(src!=singleMemoryBlock)
            std::cout<<"error1 "<<singleMemoryBlock<<" "<<src<<std::endl;
#endif
        //copy center
        for(int j=0; j<height; j++)
        {
            memcpy(dest, src, color*sizeof(T));
            dest+=color;
#ifdef DEBUG
            if(src!=singleMemoryBlock+j*width*color)
                std::cout<<"error2 "<<singleMemoryBlock<<" "<<src<<std::endl;
#endif
            memcpy(dest, src, blockWidth*sizeof(T));
            dest+=blockWidth;
            src+=blockWidth;

            src-=color;
            memcpy(dest, src, color*sizeof(T));
            dest+=color;
            src+=color;
        }
#ifdef DEBUG
        if(src!=singleMemoryBlock+height*width*color)
            std::cout<<"error2 "<<singleMemoryBlock<<" "<<src<<std::endl;
#endif

        //copy last rows
        src-=blockWidth;

        memcpy(dest, src, color*sizeof(T));
        dest+=color;

        memcpy(dest, src, blockWidth*sizeof(T));
        dest+=blockWidth;
        src+=blockWidth;

        src-=color;
        memcpy(dest, src, color*sizeof(T));
        dest+=color;
        src+=color;

#ifdef DEBUG
        if(src!=singleMemoryBlock+height*width*color)
            std::cout<<"error3 "<<singleMemoryBlock<<" "<<src<<std::endl;
#endif
        return newArray;
    }

    WorkingImage()
    {

    }
    const static int maxAmount = USHRT_MAX;
    const static int color=N;

};


#endif // WORKINGIMAGE_H
