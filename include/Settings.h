#ifndef SETTINGS_H
#define SETTINGS_H
#include <iostream>
enum class prefilterType
{
    none=0,
    statistic=1,
    median=2
};
enum class postfilterType
{
    none=0,
    impulseFilter=1,
    impulseFilterWithMedian=2,
    impulseFilterBilinear=4,
    medianFilter=3
};

enum class colorMatrixType
{
    none=0,
    dcrawDefault=1
};
enum class demosaicMethod
{
    nearestNeighbor=0,
    bilinear=1,
    simplegradients=2,
    VNG=3
};

class Settings
{

public:


    static Settings& getInstance();
    const char * getDemosaicName()
    {
        int t = static_cast<int>(demosaic);
        if(t<demosaicMethodsSize)
            return demosaicMethods[t];
        else
            return "";
    }
    virtual ~Settings();
    //data defaults
    unsigned int blackLevel=0;
    bool customBlackLevel=false;
    prefilterType prefilter=prefilterType::none;
    unsigned int prefilterMask=5;
    postfilterType postfilter=postfilterType::none;
    unsigned int postfilterMask=7;
    unsigned int saturationLevel;
    bool customSaturationLevel=false;
    colorMatrixType colorMatrix=colorMatrixType::dcrawDefault;
    demosaicMethod demosaic=demosaicMethod::VNG;
    bool verbose=true;
    std::string * outputdir=nullptr;
    static std::string fileNameOnly(std::string * file);
    static std::string removeExtension(std::string * file);
    #ifdef _WIN32
    const static char dirSymbol='\\';
    #else
    const static char dirSymbol='/';
    #endif
protected:
    Settings();

private:
    static const int demosaicMethodsSize=4;
    const char* demosaicMethods[demosaicMethodsSize] =
    {
        "nearestNeighbor","bilinear","simplegradients", "VNG"
    };

};

#endif // SETTINGS_H
