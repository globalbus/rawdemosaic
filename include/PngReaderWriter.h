#ifndef WRITEIMAGE_H
#define WRITEIMAGE_H
#include <iostream>
#include "WorkingImage.h"
class PngReaderWriter
{
public:
    PngReaderWriter();
    WorkingImage<arrayType,3> * read(std::string * file);
    void save(unsigned char **outputBuffer, int width, int height, std::string * filename);
    void saveNew(unsigned short* outputBuffer, int width, int height, std::string * filename);
    //getters/setters
    int getPrecision();
    void setPrecision(int precision);
    bool isLinearRGB();
    void setLinearRGB(bool linearRGB);
private:
    int precision=16;
    bool linearRGB=true;
};

#endif // WRITEIMAGE_H
