#ifndef STATISTICS_H
#define STATISTICS_H
#include "WorkingImage.h"
#include <fstream>
using namespace std;
class Statistics
{
public:
    class Quality
    {
    public:
        Quality();
        template<typename T>
        void writeTo(T text)
        {
            csvFile<<text;
            csvFile<<delimiter;
        }
        virtual ~Quality();
        void next();
    private:
        ofstream csvFile;
        const char delimiter =',';
        const char * firstColumn = "algorithmName,filename,MAE,MSE,PeakToNoise,FalseColors,FalseColorsWithFalsePositives\n";

    };
    Statistics(string file, WorkingImage<arrayType, 3> * original, WorkingImage<arrayType, 3> * demosaiced);
    virtual ~Statistics();
    void doAll();
    double meanSquareError=0.0;
    double meanAbsoluteError=0.0;
    double peakToNoiseRatio=0.0;
    int falseColors=0;
    static void diff(string * file1, string* file2);
    static Quality& getInstance()
    {
        static Quality instance;
        return instance;
    }
protected:
private:
    WorkingImage<arrayType,3> *original=nullptr;
    WorkingImage<arrayType,3> *demosaiced=nullptr;
    int height, width;
    static void spreadBits(WorkingImage<arrayType,3> * data);
    string file;
};

#endif // STATISTICS_H
