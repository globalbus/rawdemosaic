#include <iostream>
#include <cstdlib>
#include "LibRawWrapper.h"
#include "getopt.h"
#include "RawImage.h"
#include "Scenario.h"
using namespace std;

void printHelp()
{
    cout<<"Usage: "<<endl;
    cout<<"RawCLI [options...] file ...[file2...]"<<endl;
cout<<"Demosaic options:"<<endl;
    cout<<"\t--demosaic=n\t select demosaic method, where n is number 0-3"<<endl;
    cout<<"\t\t\t 0 for Nearest Neighbor demosaic"<<endl;
    cout<<"\t\t\t 1 for Bilinear demosaic"<<endl;
    cout<<"\t\t\t 2 for Simple Gradients demosaic"<<endl;
    cout<<"\t\t\t 3 for VNG demosaic [default]"<<endl;

    cout<<"Color handling options:"<<endl;
    cout<<"\t--black-level=n"<<endl;
    cout<<"\t\tarbitary set black level value, where n is unsigned integer"<<endl;
    cout<<"\t--saturation-level=n"<<endl;
    cout<<"\t\tarbitary set saturation level value, where n is unsigned integer"<<endl;
    cout<<"\t--color-matrix=n"<<endl;
    cout<<"\t\tselect color matrix handling method, where n is number 0-1"<<endl;
    cout<<"\t\t\t 0 for none"<<endl;
    cout<<"\t\t\t 1 for dcraw default [default]"<<endl;
    cout<<"Filtering options:"<<endl;
    cout<<"\t--prefilter-method=n"<<endl;
    cout<<"\t\tselect prefilter method, where n is number 0-2"<<endl;
    cout<<"\t\t\t 0 for none [default]"<<endl;
    cout<<"\t\t\t 1 for statistics filtering"<<endl;
    cout<<"\t\t\t 2 for median filtering"<<endl;

    cout<<"\t--postfilter-method=n"<<endl;
    cout<<"\t\tselect postfilter method, where n is number 0-3"<<endl;
    cout<<"\t\t\t 0 for none [default]"<<endl;
    cout<<"\t\t\t 1 for nonlinear filter"<<endl;
    cout<<"\t\t\t 2 for nonlinear filter with median correction"<<endl;
    cout<<"\t\t\t 3 for median filter"<<endl;

    cout<<"\t--prefilter-mask=n"<<endl;
    cout<<"\t\tselect prefilter mask, where n is odd number [5 by default]"<<endl;
    cout<<"\t--postfilter-mask=n"<<endl;
    cout<<"\t\tselect postfilter mask, where n is odd number [7 by default]"<<endl;

    cout<<"Misc options:"<<endl;
    cout<<"\t--verbose\t additional output"<<endl;
    cout<<"\t--help\t\t output this info"<<endl;
}
int main(int argc, char* argv[])
{

    struct option long_options[] =
    {
        {"verbose", no_argument,0, 1},
        {"help",   no_argument,0, 2},
        {"postfilter-mask",  required_argument, 0, 3},
        {"postfilter-method",  required_argument, 0, 4},
        {"prefilter-mask",  required_argument, 0, 5},
        {"prefilter-method",  required_argument, 0, 6},
        {"black-level",  required_argument, 0, 7},
        {"color-matrix",  required_argument, 0, 8},
        {"demosaic", required_argument, 0, 9},
        {"saturation-level",  required_argument, 0, 10},
        {"output-dir",  required_argument, 0, 11},
        {0, 0, 0, 0}
    };
    while(true)
    {
        int option_index=0;
        int c = getopt_long (argc, argv, "",long_options, &option_index);
        if (c == -1)
            break;
        switch(c)
        {
        case 1:
            Settings::getInstance().verbose=true;
            break;
        case 2:
            printHelp();
            return 0;
            break;
        case 3:
            Settings::getInstance().postfilterMask=atoi(optarg);
            break;
        case 4:
            Settings::getInstance().postfilter=static_cast<postfilterType>(atoi(optarg));
            break;
        case 5:
            Settings::getInstance().prefilterMask=atoi(optarg);
            break;
        case 6:
            Settings::getInstance().prefilter=static_cast<prefilterType>(atoi(optarg));
            break;
        case 7:
            Settings::getInstance().blackLevel=atoi(optarg);
            Settings::getInstance().customBlackLevel=true;
            break;
        case 8:
            Settings::getInstance().colorMatrix=static_cast<colorMatrixType>(atoi(optarg));
            break;
        case 9:
            Settings::getInstance().demosaic=static_cast<demosaicMethod>(atoi(optarg));
            break;
        case 10:
            Settings::getInstance().saturationLevel=atoi(optarg);
            Settings::getInstance().customSaturationLevel=true;
            break;
        case 11:
            Settings::getInstance().outputdir=new string(optarg);
            break;
        }

    }
    if (optind < argc)
    {
        LibRawWrapper * wrapper = new LibRawWrapper();
        Scenario scenario(wrapper);
        while (optind < argc)
        {
            string * file = new string(argv[optind++]);
            scenario.launchScenario(file);
            delete file;
        }
        delete wrapper;
    }
    else
        printHelp();
    return 0;
}

