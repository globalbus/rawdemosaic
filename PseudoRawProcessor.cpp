#include <iostream>
#include <vector>
#include "NearestNeighborScenario.h"
#include "BilinearScenario.h"
#include "Scenario.h"
#include "SimpleGradientsScenario.h"
#include "BilinearOldScenario.h"
#include "VNGScenario.h"
#include "PseudoRawWrapper.h"
#include "Statistics.h"
using namespace std;

int main(int argc, char* argv[])
{
    if(argc>1){
        Statistics * stats;
        vector<Scenario*> scenarios;
        string * file = new string(argv[1]);
        PseudoRawWrapper * wrapper= new PseudoRawWrapper();
        //scenarios.push_back(new NearestNeighborScenario(wrapper));
        //scenarios.push_back(new BilinearScenario(wrapper));
        scenarios.push_back(new SimpleGradientsScenario(wrapper));
        //scenarios.push_back(new BilinearOldScenario());
        scenarios.push_back(new VNGScenario(wrapper));
        for(uint i=0; i<scenarios.size();i++){
            scenarios[i]->launchScenario(file);

            stats= new Statistics(scenarios[i]->removeExtension(file),wrapper->originalImage,scenarios[i]->getDemosaicedImage());
            stats->getInstance().writeTo(scenarios[i]->algorithmName);
            stats->doAll();
            delete stats;
            delete scenarios[i];
        }
        delete file;
        delete wrapper;
    }
    cout << "Exiting!" << endl;
    return 0;
}

