#include <iostream>
#include "Statistics.h"
using namespace std;

int main(int argc, char* argv[])
{
    if(argc==3){
        string * file1 = new string(argv[1]);
        string * file2 = new string(argv[2]);
        Statistics::diff(file1, file2);
        delete file1;
        delete file2;
    }
    cout << "Exiting!" << endl;
    return 0;
}

