#include "PngReaderWriter.h"
#include <png.h>
#include <iostream>
#include <cstring>

using namespace std;
PngReaderWriter::PngReaderWriter()
{

}
//Save using old API
void PngReaderWriter::save(unsigned char** outputBuffer,int width, int height, string * filename)
{
    FILE *fp; //pointer to file
    png_structp png_ptr; //data structure
    png_infop info_ptr; //info structure

    if ((fp = fopen(filename->c_str(), "wb")) == nullptr)//open file
    {
        fprintf(stderr, "Could not open file %s for writing\n", filename->c_str());
        return;
    }

    // Initialize write structure
    if ((png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, nullptr, nullptr, nullptr)) == nullptr)
    {
        fprintf(stderr, "Could not allocate write struct\n");
        return;
    }

    // Initialize info structure
    if ((info_ptr =png_create_info_struct(png_ptr)) == nullptr)
    {
        fprintf(stderr, "Could not allocate info struct\n");
        return;
    }

    if (setjmp(png_jmpbuf(png_ptr)))
    {
        fprintf(stderr, "Error during png creation\n");
        return;
    }
    png_init_io(png_ptr, fp);
    //disable compression
    //png_set_compression_level(png_ptr,
     //                         Z_NO_COMPRESSION);
    // Write header (8 bit colour depth)
    png_set_IHDR(png_ptr, info_ptr, width, height,
                 8, PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE,
                 PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);
    //set data
    png_set_rows (png_ptr, info_ptr, outputBuffer);
    //write changes
    png_write_png (png_ptr, info_ptr, PNG_TRANSFORM_IDENTITY, nullptr);
    //save changes
    if (setjmp(png_jmpbuf(png_ptr)))
    {
        fprintf(stderr, "Error during png creation\n");
        return;
    }
    //do cleanup
    png_destroy_write_struct (&png_ptr, &info_ptr);
    fclose (fp);
    return;
}
WorkingImage<arrayType,3> * PngReaderWriter::read(std::string * file){
    png_image image;
    memset(&image, 0, sizeof image);
    image.version = PNG_IMAGE_VERSION;
    if(!png_image_begin_read_from_file(&image, file->c_str()))
        return nullptr;
    if(linearRGB)
        image.format=PNG_FORMAT_LINEAR_RGB;//libpng do conversion to sRGB by itself
    else
        image.format=PNG_FORMAT_LINEAR_Y;
    int image_size =PNG_IMAGE_SIZE(image);
    std::cout<<"size is "<<image_size<<" "<<image.message<<std::endl;
    std::cout<<image.height<<" "<<image.width<<std::endl;
    png_bytep * buffer = new png_bytep[image_size];
    png_image_finish_read(&image, NULL/*background*/, buffer,
               0/*row_stride*/, NULL/*colormap for PNG_FORMAT_FLAG_COLORMAP */);
    WorkingImage<ushort,3> * temp = new WorkingImage<ushort,3>((ushort*)buffer, image.width, image.height);
    WorkingImage<arrayType,3> * ret=new WorkingImage<arrayType,3>(temp);
    delete temp;
    return ret;
}
//Save using new API
void PngReaderWriter::saveNew(unsigned short* outputBuffer,int width, int height, string * filename)
{
    png_image image;
    memset(&image, 0, sizeof image);
    if(linearRGB)
        image.format=PNG_FORMAT_LINEAR_RGB;//libpng do conversion to sRGB by itself
    else
        image.format=PNG_FORMAT_LINEAR_Y;
    image.height=height;
    image.width=width;
    image.version=PNG_IMAGE_VERSION;
    if (png_image_write_to_file(&image, filename->c_str(),true/*convert_to_8bit*/, outputBuffer, 0/*row_stride - byte order*/,nullptr)==0)
        fprintf(stderr, "pngtopng: write %s: %s\n", filename->c_str(), image.message);
    return;
}
//getters/setters
int PngReaderWriter::getPrecision()
{
    return precision;
}
void PngReaderWriter::setPrecision(int precision)
{
    if(precision==8||precision==16)
        this->precision=precision;
}
bool PngReaderWriter::isLinearRGB(){
    return linearRGB;
}
void PngReaderWriter::setLinearRGB(bool linearRGB){
    this->linearRGB=linearRGB;
}
