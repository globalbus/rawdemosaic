#include "Settings.h"

using namespace std;
Settings::Settings()
{
    //ctor
}

Settings::~Settings()
{
    if(outputdir!=nullptr)
        delete outputdir;
}
Settings& Settings::getInstance()
    {
        static Settings instance;
        return instance;
    }
string Settings::removeExtension(string * file)
{
    unsigned int i = file->find_last_of('.');
    if (i > 0 && i < file->length() - 1)
        return file->substr(0,i);
    return *file;
}
string Settings::fileNameOnly(string * file)
{
    string s = removeExtension(file);
    unsigned int i = file->find_last_of(dirSymbol);
    if (i > 1 && i < file->length() - 1)
        return file->substr(i+1,s.length()-i-1);
    return s;
}
