#include "RawWrapper.h"
#include <cstring>
#include "Settings.h"
using namespace std;
RawWrapper::RawWrapper()
{
    //ctor
}

RawWrapper::~RawWrapper()
{
    free();

}
void RawWrapper::setExtendValue(){
    extend =2;
    if(Settings::getInstance().prefilter!=prefilterType::none)
        extend = max(extend, (Settings::getInstance().prefilterMask+1)/2);
    if(Settings::getInstance().postfilter==postfilterType::impulseFilterWithMedian
            ||Settings::getInstance().postfilter==postfilterType::impulseFilter
            ||Settings::getInstance().postfilter==postfilterType::impulseFilterBilinear)
        extend = max(extend, (Settings::getInstance().postfilterMask+1)/2);
    if(extend&1)
        extend++;
    cout<<"extend "<<extend<<endl;
}
RawWrapper::RawWrapper(RawWrapper& source){
    height=source.height;
    width=source.width;
    extend=source.extend;
    int size=(height+(extend<<1))*(width+(extend<<1));
    extendedRawImage = new ushort[size];
    memcpy(extendedRawImage ,source.extendedRawImage, size*sizeof(ushort));
    buildArray();
}
void RawWrapper::free(){
    if(extendRawArray!=nullptr)
    {
        delete[] extendedRawImage;
        delete[] (extendRawArray-extend);
        extendedRawImage=nullptr;
        extendRawArray=nullptr;
    }
}
void RawWrapper::buildArray(){
    extendRawArray= new ushort*[height+(extend<<1)];
    for(int j=0, l=extend; j<height+(extend<<1); j++)
    {
        extendRawArray[j]=&extendedRawImage[l];
        l+=width+(extend<<1);
    }
    extendRawArray=extendRawArray+extend;
}


ushort * RawWrapper::extendBayerArea(ushort* data, int extendBlocks)
{
    ushort * newArray = new ushort[(height+(extendBlocks<<2))*(width+(extendBlocks<<2))];
    int blockWidth=width;
    //copy first rows
    ushort * src= data+(blockWidth*(extendBlocks-1)<<1);//data
    ushort * dest=newArray;
    for(int j=0; j<extendBlocks; j++)
    {
        src+=(extendBlocks-1)<<1;//0
        for(int i=0; i<extendBlocks; i++)
        {
            memcpy(dest, src, sizeof(ushort)<<1);
            dest+=2;
            src-=2;//-2
        }
        src+=2;//0

        memcpy(dest, src, blockWidth*sizeof(ushort));
        dest+=blockWidth;
        src+=blockWidth;//width
        src-=2;//width-2
        for(int i=0; i<extendBlocks; i++)
        {
            memcpy(dest, src, sizeof(ushort)<<1);
            dest+=2;
            src-=2;//width-4
        }
        src+=(extendBlocks+1)<<1;//width

        src+=(extendBlocks-1)<<1;//width
        for(int i=0; i<extendBlocks; i++)
        {
            memcpy(dest, src, sizeof(ushort)<<1);
            dest+=2;
            src-=2;//width-2
        }
        src+=2;//width

        memcpy(dest, src, blockWidth*sizeof(ushort));
        dest+=blockWidth;
        src+=blockWidth;//2*width

        src-=2;//2*width-2
        for(int i=0; i<extendBlocks; i++)
        {
            memcpy(dest, src, sizeof(ushort)<<1);
            dest+=2;
            src-=2;//2*width-4
        }
        src+=(extendBlocks+1)<<1;//2*width

        src-=blockWidth<<2;//-2*width
    }
    src+=blockWidth<<1;//data
#ifdef DEBUG
    if(src!=data)
        cout<<"error1 "<<data<<" "<<src<<endl;
#endif
    //copy center
    for(uint j=0; j<height; j++)
    {
        src+=(extendBlocks-1)<<1;
        for(int i=0; i<extendBlocks; i++)
        {
            memcpy(dest, src, sizeof(ushort)<<1);
            dest+=2;
            src-=2;
        }
        src+=2;
#ifdef DEBUG
        if(src!=data+j*width)
            cout<<"error2 "<<data<<" "<<src<<endl;
#endif
        memcpy(dest, src, blockWidth*sizeof(ushort));
        dest+=blockWidth;
        src+=blockWidth;

        src-=2;
        for(int i=0; i<extendBlocks; i++)
        {
            memcpy(dest, src, sizeof(ushort)<<1);
            dest+=2;
            src-=2;
        }
        src+=(extendBlocks+1)<<1;
    }
#ifdef DEBUG
    if(src!=data+height*width)
        cout<<"error2 "<<data<<" "<<src<<endl;
#endif
    //copy last rows
    src-=blockWidth<<1;
    for(int j=0; j<extendBlocks; j++)
    {
        src+=(extendBlocks-1)<<1;
        for(int i=0; i<extendBlocks; i++)
        {
            memcpy(dest, src, sizeof(ushort)<<1);
            dest+=2;
            src-=2;
        }
        src+=2;

        memcpy(dest, src, blockWidth*sizeof(ushort));
        dest+=blockWidth;
        src+=blockWidth;
        src-=2;
        for(int i=0; i<extendBlocks; i++)
        {
            memcpy(dest, src, sizeof(ushort)<<1);
            dest+=2;
            src-=2;
        }
        src+=(extendBlocks+1)<<1;

        src+=(extendBlocks-1)<<1;
        for(int i=0; i<extendBlocks; i++)
        {
            memcpy(dest, src, sizeof(ushort)<<1);
            dest+=2;
            src-=2;
        }
        src+=2;

        memcpy(dest, src, blockWidth*sizeof(ushort));
        dest+=blockWidth;
        src+=blockWidth;
        src-=2;
        for(int i=0; i<extendBlocks; i++)
        {
            memcpy(dest, src, sizeof(ushort)<<1);
            dest+=2;
            src-=2;
        }
        src+=(extendBlocks+1)<<1;

        src-=blockWidth<<2;
    }
#ifdef DEBUG
    src+=blockWidth*(extendBlocks+1)*2;
    if(src!=data+height*width)
        cout<<"error3 "<<data<<" "<<src<<endl;
#endif
    return newArray;
}
ushort * RawWrapper::extendBayerArea(ushort* data)
{
    ushort * newArray = new ushort[(height+4)*(width+4)];
    int blockWidth=width;

    //copy first rows
    ushort * src= data;//data
    ushort * dest=newArray;

    memcpy(dest, src, sizeof(ushort)<<1);
    dest+=2;
    memcpy(dest, src, blockWidth*sizeof(ushort));
    dest+=blockWidth;
    src+=blockWidth-2;//width-2
    memcpy(dest, src, sizeof(ushort)<<1);
    dest+=2;
    src+=2;//width

    memcpy(dest, src, sizeof(ushort)<<1);
    dest+=2;
    memcpy(dest, src, blockWidth*sizeof(ushort));
    dest+=blockWidth;
    src+=blockWidth-2;//2*width-2
    memcpy(dest, src, sizeof(ushort)<<1);
    dest+=2;
    src+=2;//2*width

    src-=blockWidth<<1;//-2*width
#ifdef DEBUG
    if(src!=data)
        cout<<"error1 "<<data<<" "<<src<<endl;
#endif
    //copy center
    for(uint j=0; j<height; j++)
    {
        memcpy(dest, src, sizeof(ushort)<<1);
        dest+=2;
#ifdef DEBUG
       if(src!=data+j*width)
            cout<<"error2 "<<data<<" "<<src<<endl;
#endif
        memcpy(dest, src, blockWidth*sizeof(ushort));
        dest+=blockWidth;
        src+=blockWidth-2;
        memcpy(dest, src, sizeof(ushort)<<1);
        dest+=2;
        src+=2;
    }
#ifdef DEBUG
    if(src!=data+height*width)
        cout<<"error2 "<<data<<" "<<src<<endl;
#endif
    //copy last rows
    src-=blockWidth<<1;

    memcpy(dest, src, sizeof(ushort)<<1);
    dest+=2;
    memcpy(dest, src, blockWidth*sizeof(ushort));
    dest+=blockWidth;
    src+=blockWidth-2;//width
    memcpy(dest, src, sizeof(ushort)<<1);
    dest+=2;
    src+=2;//width

    memcpy(dest, src, sizeof(ushort)<<1);
    dest+=2;
    memcpy(dest, src, blockWidth*sizeof(ushort));
    dest+=blockWidth;
    src+=blockWidth-2;//2*width
    memcpy(dest, src, sizeof(ushort)<<1);
#ifdef DEBUG
    dest+=2;
    src+=2;//2*width
    if(src!=data+height*width)
        cout<<"error3 "<<data<<" "<<src<<endl;
#endif
    return newArray;
}
