#include "Scenario.h"
#include <sys/types.h>
#include <sys/stat.h>
using namespace std;
Scenario::Scenario(RawWrapper * rawWrapper)
{
    if(rawWrapper!=nullptr)
        RAWEngine = new RAWImage(rawWrapper);
}

Scenario::~Scenario()
{
    delete RAWEngine;
}

WorkingImage<arrayType, 3> * Scenario::getDemosaicedImage()
{
    return RAWEngine->image;
}

Scenario::Times::Times()
{

    csvFile.open("stats.csv", fstream::app);
    csvFile.seekp(0, std::ios_base::end);
    if(csvFile.tellp()==0)
        csvFile<<firstColumn;
}

Scenario::Times::~Times()
{
    csvFile.close();
}
void Scenario::Times::next()
{
    csvFile<<"\n";
    csvFile.flush();
}
void Scenario::launchScenario(string * file)
{
    string s;
    if(Settings::getInstance().outputdir==nullptr)
        s = Settings::removeExtension(file)+"_"+Settings::getInstance().getDemosaicName()+".png";
    else
    {
        string outputDir=*Settings::getInstance().outputdir;
#ifndef _WIN32
#include <sys/types.h>
#include <sys/stat.h>
        struct stat info;
        if( stat( outputDir.c_str(), &info ) != 0)
            mkdir(outputDir.c_str(), 0755);
        else if( info.st_mode & !S_IFDIR)
            mkdir((outputDir+"_").c_str(), 0755);//if exists file with this name
#endif
        if(outputDir[outputDir.size()-1]!=Settings::dirSymbol)
            outputDir+=Settings::dirSymbol;
        s = outputDir+Settings::fileNameOnly(file)+"_"+Settings::getInstance().getDemosaicName()+".png";
    }
    getInstance().writeTo(Settings::fileNameOnly(file));
    algorithmName=Settings::getInstance().getDemosaicName();
    getInstance().writeTo(algorithmName);
    int time=0;
    time = Timer::getExecutionTimeInMilliseconds(*RAWEngine, &RAWImage::open, (string *)file);
    getInstance().writeTo(time);
    RAWEngine->cameraWhiteBalance();
    time = Timer::getExecutionTimeInMilliseconds(*RAWEngine, (void (RAWImage::*)())(&RAWImage::copyForProcess));
    time += Timer::getExecutionTimeInMilliseconds(*RAWEngine, &RAWImage::applyPrefilter);
    getInstance().writeTo(time);
    time = Timer::getExecutionTimeInMilliseconds(*RAWEngine, &RAWImage::demosaic);
    getInstance().writeTo(time);
    time = Timer::getExecutionTimeInMilliseconds(*RAWEngine, &RAWImage::applyPostfilter);
    time += Timer::getExecutionTimeInMilliseconds(*RAWEngine, &RAWImage::colorCorrection);
    getInstance().writeTo(time);
    time = Timer::getExecutionTimeInMilliseconds(*RAWEngine, &RAWImage::savePNG, &s);
    getInstance().writeTo(time);
    getInstance().next();
}
