#include "LibRawWrapper.h"
#include <iostream>
#include <algorithm>
using namespace std;

LibRawWrapper::LibRawWrapper()
{
    //ctor
}

LibRawWrapper::~LibRawWrapper()
{
    //dtor
}
void LibRawWrapper::free()
{
    iProcessor.recycle();
    RawWrapper::free();
}

int LibRawWrapper::open(std::string * file)
{
    free();
    int ret;
    if( (ret = iProcessor.open_file(file->c_str())) != LIBRAW_SUCCESS)
    {
        fprintf(stderr,"Cannot open %s: %s\n",file->c_str(),libraw_strerror(ret));
        return ret;
    }
    if( (ret = iProcessor.unpack() ) != LIBRAW_SUCCESS)
    {
        fprintf(stderr,"Cannot unpack %s: %s\n",file->c_str(),libraw_strerror(ret));
        return ret;
    }
    decodeOffsets(0);
    cout<<file->c_str()<<" - Image loaded"<<endl;
    int black;
    if(Settings::getInstance().customBlackLevel)
    {
        black = Settings::getInstance().blackLevel;
        if(Settings::getInstance().verbose)
            cout<<"Using custom black level "<<black<<endl;
    }
    else
    {
        black =iProcessor.imgdata.color.black;
        if(Settings::getInstance().verbose)
            cout<<"Using automatic black level "<< black<<endl;
    }
    for(int j=0; j<4; j++)
        blackLevels[j]=iProcessor.imgdata.color.cblack[j]+black;
    return 0;

}
void LibRawWrapper::decodeOffsets(int i)
//@param index to start
{
    int value = iProcessor.COLOR(i,i);
    colorOffsets[value].x=0;
    colorOffsets[value].y=0;
    //cout<<value<<endl;
    bayerOffsets[0]=value;
    value = iProcessor.COLOR(i,i+1);
    colorOffsets[value].x=0;
    colorOffsets[value].y=1;
    //cout<<value<<endl;
    bayerOffsets[1]=value;
    value = iProcessor.COLOR(i+1,i);
    colorOffsets[value].x=1;
    colorOffsets[value].y=0;
    //cout<<value<<endl;
    bayerOffsets[2]=value;
    value = iProcessor.COLOR(i+1,i+1);
    colorOffsets[value].x=1;
    colorOffsets[value].y=1;
    //cout<<value<<endl;
    bayerOffsets[3]=value;
}
void LibRawWrapper::doWhiteBalance()
{
    auto color = iProcessor.imgdata.color;
    if(color.cam_mul[0]*color.cam_mul[1]*color.cam_mul[2]>0)
    {
        if(Settings::getInstance().verbose)
            cout<<"using camera white balance"<<endl;
        multipliers=iProcessor.imgdata.color.cam_mul;
    }
    else
    {
        if(Settings::getInstance().verbose)
            cout<<"using libraw white balance"<<endl;
        multipliers=iProcessor.imgdata.color.pre_mul;

    }
    if(multipliers[3]==0.0)//for RGBG arrays
        multipliers[3]=multipliers[1]; //copy green to green
    //multipliers normalization
//    cout<<iProcessor.imgdata.color.rgb_cam[0][0]<<" "<<iProcessor.imgdata.color.rgb_cam[0][1]<<" "<<iProcessor.imgdata.color.rgb_cam[0][2]<<endl;
//    cout<<iProcessor.imgdata.color.rgb_cam[1][0]<<" "<<iProcessor.imgdata.color.rgb_cam[1][1]<<" "<<iProcessor.imgdata.color.rgb_cam[1][2]<<endl;
//    cout<<iProcessor.imgdata.color.rgb_cam[2][0]<<" "<<iProcessor.imgdata.color.rgb_cam[2][1]<<" "<<iProcessor.imgdata.color.rgb_cam[2][2]<<endl;
    //cout<<iProcessor.imgdata.color.rgb_cam[3][0]<<" "<<iProcessor.imgdata.color.rgb_cam[3][1]<<" "<<iProcessor.imgdata.color.rgb_cam[3][2]<<endl;
    //double sum = std::accumulate(multipliers, multipliers+4, 0.0);
    //scale with green!
    //float max = multipliers[1];
    float max = *min_element(multipliers, multipliers+4);
    //double max = sum / 4;
    for(int i=0; i<4; i++)
    {
        multipliers[i]/=max;
    }
    if(Settings::getInstance().verbose)
    {
        cout<<"Color multipliers "<<iProcessor.imgdata.idata.cdesc << endl;
        cout<< multipliers[0]<<" "<<multipliers[1]<<" "<<multipliers[2]<<" "<<multipliers[3]<<endl;
    }
}
LibRawWrapper::LibRawWrapper(LibRawWrapper& source):RawWrapper(source)
{

}
double** LibRawWrapper::getColorArray()
{
    auto color = iProcessor.imgdata.color;
    double* matrixdata = new double[9];
    double** matrix= new double*[3];
    for(int j=0,l=0; j<3; j++)
    {
        matrix[j]=&matrixdata[l];
        l+=3;
    }
    for(int j=0; j<3; j++)
        for(int i=0; i<3; i++)
        {
            matrix[j][i]=color.rgb_cam[j][i];
        }
    return matrix;
}
int LibRawWrapper::maxValue()
{
    int ret;
    if(Settings::getInstance().customSaturationLevel)
    {
        ret=Settings::getInstance().saturationLevel;
        if(Settings::getInstance().verbose)
            cout<<"Using custom saturation level "<< ret<<endl;
    }
    else
    {
        ret=iProcessor.imgdata.color.maximum;
        if(Settings::getInstance().verbose)
            cout<<"Using automatic saturation level "<< ret<<endl;
    }
    return ret;
}
RawWrapper* LibRawWrapper::copy()
{
    return new LibRawWrapper(*this);
}
void LibRawWrapper::cropToActiveArea()
{
    ushort* rawImage;
    rawImage=iProcessor.imgdata.rawdata.raw_image;
    cout<<*max_element(rawImage, rawImage+iProcessor.imgdata.sizes.height*iProcessor.imgdata.sizes.width)<<endl;
    ushort * cropArea;
    setExtendValue();

    if(iProcessor.imgdata.sizes.raw_height!=iProcessor.imgdata.sizes.height
            ||iProcessor.imgdata.sizes.raw_width!=iProcessor.imgdata.sizes.width)
    {
        cropArea = new ushort[iProcessor.imgdata.sizes.height*iProcessor.imgdata.sizes.width];
        ushort * dest = cropArea;
        ushort * src = rawImage;
        src+=iProcessor.imgdata.sizes.raw_width*iProcessor.imgdata.sizes.top_margin;//skip top
        for(int j=0; j<iProcessor.imgdata.sizes.height; j++)
        {
            src+=iProcessor.imgdata.sizes.left_margin;//crop left margin
            memcpy(dest, src, iProcessor.imgdata.sizes.width*sizeof(ushort));
            dest+=iProcessor.imgdata.sizes.width;
            src+=(iProcessor.imgdata.sizes.raw_width-iProcessor.imgdata.sizes.left_margin);//crop right margin
        }

#ifdef DEBUG
        src+=iProcessor.imgdata.sizes.raw_width*(iProcessor.imgdata.sizes.raw_height
                -iProcessor.imgdata.sizes.top_margin-iProcessor.imgdata.sizes.height);
        if(src-rawImage!=iProcessor.imgdata.sizes.raw_width*iProcessor.imgdata.sizes.raw_height)
            cout<<"Error in cropping, source is not aligned"<<endl;
        if(dest-iProcessor.imgdata.sizes.height*iProcessor.imgdata.sizes.width!=cropArea)
            cout<<"Error in cropping, destination is not aligned"<<endl;
#endif
        rawImage=cropArea;
        height=iProcessor.imgdata.sizes.height;
        width=iProcessor.imgdata.sizes.width;
        extendedRawImage=extendBayerArea(rawImage,extend/2);
        delete[] cropArea;
    }
    else
    {
        height=iProcessor.imgdata.sizes.raw_height;
        width=iProcessor.imgdata.sizes.raw_width;
        extendedRawImage=extendBayerArea(rawImage,extend/2);
    }

    buildArray();

    iProcessor.free_image(); //we don't need it right now
//formula to find color code on rawArray for given j - height, i - width
#define bayerFind bayerOffsets[(i&1)|(j&1)<<1]
    //deal with bad pixels
    for(int j=0, l=((width+(extend<<1))*(extend)+extend); j <  height; j++)
    {
        for(int i=0; i < width; i++)
        {
            if(extendedRawImage[l]==0)  //zero means defected pixel - interpolate bilinear
            {
                if(bayerFind&1)
                    //green value
                    extendedRawImage[l]=(extendRawArray[j+1][i+1]+extendRawArray[j-1][i+1]+extendRawArray[j+1][i-1]+extendRawArray[j-1][i-1])>>2;
                else
                    extendedRawImage[l]=(extendRawArray[j+2][i]+extendRawArray[j-2][i]+extendRawArray[j][i+2]+extendRawArray[j][i-2])>>2;

            }
            l++;
        }
        l+=extend<<1;
    }

}
