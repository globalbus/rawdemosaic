#include "PseudoRawWrapper.h"
//colorcodes, defined with LibRAW convention
#define redCode 0
#define green1Code 1
#define blueCode 2
#define green2Code 3
#define bayerFind bayerOffsets[(i&1)|(j&1)<<1]
using namespace std;
PseudoRawWrapper::PseudoRawWrapper()
{

}

PseudoRawWrapper::~PseudoRawWrapper()
{
    free();
}
void PseudoRawWrapper::free()
{
    if(multipliers!=nullptr)
    {
        delete[] multipliers;
        multipliers=nullptr;
    }
    if(originalImage!=nullptr)
    {
        delete originalImage;
        originalImage=nullptr;
        RawWrapper::free();
    }
}
void PseudoRawWrapper::decodeOffsets(int i)
{
    int value = redCode;
    colorOffsets[value].x=0;
    colorOffsets[value].y=0;
    bayerOffsets[0]=value;
    value = green1Code;
    colorOffsets[value].x=0;
    colorOffsets[value].y=1;
    bayerOffsets[1]=value;
    value = green2Code;
    colorOffsets[value].x=1;
    colorOffsets[value].y=0;
    bayerOffsets[2]=value;
    value = blueCode;
    colorOffsets[value].x=1;
    colorOffsets[value].y=1;
    bayerOffsets[3]=value;
}
void PseudoRawWrapper::doWhiteBalance()
{
    multipliers = new float[4];
    for(int j=0; j<4; j++)
        multipliers[j] = 1;

}
PseudoRawWrapper::PseudoRawWrapper(PseudoRawWrapper& source):RawWrapper(source){

}
RawWrapper* PseudoRawWrapper::copy(){
    return new PseudoRawWrapper(*this);
}
double** PseudoRawWrapper::getColorArray()
{
    double* matrixdata = new double[9];
    double** matrix= new double*[3];
    for(int j=0,l=0; j<3; j++)
    {
        matrix[j]=&matrixdata[l];
        l+=3;
    }
        memcpy(matrixdata,noConv,sizeof(double)*9);
        return matrix;
}
int PseudoRawWrapper::open(std::string * file)
{
    free();
    PngReaderWriter reader;
    originalImage = reader.read(file);
    if(originalImage==nullptr)
        return 1;
    width=originalImage->width;
    height=originalImage->height;
    decodeOffsets(0);
    cout<<file->c_str()<<" - Image loaded"<<endl;

    for(int j=0; j<4; j++)
        blackLevels[j]=0;
    return 0;
}
int PseudoRawWrapper::maxValue(){
    return (1<<16)-1;
}
void PseudoRawWrapper::cropToActiveArea()
{
    ushort * rawImage = new ushort[height*width];
    //arrayType*** colors = originalImage->colors;
    for(uint j=0, k, l=0; j<height; j++)
        for(uint i=0; i<width; i++)
        {
            k=bayerFind;
            if(k&1)//green
                rawImage[l]=originalImage->getG1(j, i);
            else
                rawImage[l]=originalImage->get(j, i,k);
            l++;
        }
    setExtendValue();
    extendedRawImage=extendBayerArea(rawImage,extend/2);
    delete[] rawImage;
    buildArray();
}
