#include "RAWImageOld.h"
#include <cstring>

//redcodes, defined with LibRAW convention
#define redCode 0
#define green1Code 1
#define blueCode 2
#define green2Code 3

#define offsetRed colorOffsets[redCode]
#define offsetGreen1 colorOffsets[green1Code]
#define offsetBlue colorOffsets[blueCode]
#define offsetGreen2 colorOffsets[green2Code]

//fast access methods to colors - OLD WAY
#define red colors[redCode]
#define green1 colors[green1Code]
#define blue colors[blueCode]
#define green2 colors[green2Code]

using namespace std;
RAWImageOld::RAWImageOld(RawWrapper * sourceWrapper) : RAWImage(sourceWrapper)
{
    //ctor
}

RAWImageOld::~RAWImageOld()
{
    if(colors!=nullptr)
    {
        delete[] singleMemoryBlock;
        delete[] colors[0];
        delete[] colors;
    }
}
void RAWImageOld::open(string * file)
{
    RAWImage::open(file);
}
void RAWImageOld::medianFilter(int maskSize)
{
    RAWImage::medianFilter(maskSize);
}
void RAWImageOld::copyForProcess()
{
    if(!blocked)
    {
        blocked=true;
        sourceWrapper->cropToActiveArea();
        height=sourceWrapper->height;
        width=sourceWrapper->width;
        extend=sourceWrapper->extend;
        extendedRawImage=sourceWrapper->extendedRawImage;
        extendRawArray=sourceWrapper->extendRawArray;
        bayerOffsets=sourceWrapper->bayerOffsets;
        colorOffsets=sourceWrapper->colorOffsets;
        int * blackLevels = sourceWrapper->blackLevels;
#define colorSize 3
        imageSize = height*width*colorSize;
        // 3 dimension array for data
        colors = new arrayType**[colorSize];
        //allocate single memory block for image, it's much more faster than allocate many blocks for single row/column
        singleMemoryBlock = new arrayType[imageSize];
        //single memory block for columns
        colors[0]= new arrayType*[height*colorSize];
        for(uint j=0, l=0; j<colorSize; j++)
        {
            colors[j] = &colors[0][0]+j*height;
            for(uint i = 0; i < height; ++i)
            {
                colors[j][i]=&singleMemoryBlock[l];
                l+=width;
            }
        }
        memset(singleMemoryBlock, 0, sizeof(arrayType)*imageSize);



        //deal with bad pixels
        for(int j=0, l=extend; j <  height; j++)
        {
            for(int i=0; i < width; i++)
            {
                if(extendedRawImage[l]==0)  //zero means defected pixel - interpolate bilinear
                {
                    if(bayerFind(j,i)&1)
                        //green value
                        extendedRawImage[l]=(extendRawArray[j+1][i+1]+extendRawArray[j-1][i+1]+extendRawArray[j+1][i-1]+extendRawArray[j-1][i-1])>>2;
                    else

                        extendedRawImage[l]=(extendRawArray[j+2][i]+extendRawArray[j-2][i]+extendRawArray[j][i+2]+extendRawArray[j][i-2])>>2;
                }
                l++;
            }
            l+=extend*2;
        }
        float* multipliers = sourceWrapper->multipliers;
        for(int j=0,k, l =0; j <  height+2*extend; j++)
        {
            for(int i=0; i < width+2*extend; i++)
            {
                k=bayerFind(j,i);
                //substract average black current
                if(extendedRawImage[l]<blackLevels[k])//fail-safe
                    extendedRawImage[l]=0;
                else
                    extendedRawImage[l]-=blackLevels[k];

                extendedRawImage[l]*=multipliers[k];
                l++;
            }
        }
//        for(int j=0,k, l =extend; j <  height; j++)
//        {
//            for(int i=0; i < width; i++)
//            {
//                k=bayerFind(j,i);
//
//                colors[k][j][i]=extendedRawImage[l];
//                l++;
//            }
//            l+=extend*2;
//        }

        cout<<"image balanced and copied"<<endl;
        blocked=false;
    }

}
void RAWImageOld::nearestNeighborDemosaic()
{
    if(!blocked)
    {
        blocked=true;
        //Nearest Neighbor demosaicing
        for(int j = 0; j <  height; j+=2)
            for(int i = 0; i < width; i+=2)
            {
                int temp =extendRawArray[j+colorOffsets[redCode].x][i+colorOffsets[redCode].y];
                red[j][i]=temp;
                red[j][i+1]=temp;
                red[j+1][i]=temp;
                red[j+1][i+1]=temp;
                int off = colorOffsets[green1Code].x;
                temp =extendRawArray[j+colorOffsets[green1Code].x][i+colorOffsets[green1Code].y];
                green1[j+off][i]=temp;
                green1[j+off][i+1]=temp;

                temp =extendRawArray[j+colorOffsets[blueCode].x][i+colorOffsets[blueCode].y];
                blue[j][i]=temp;
                blue[j][i+1]=temp;
                blue[j+1][i]=temp;
                blue[j+1][i+1]=temp;
                off = colorOffsets[green2Code].x;
                temp =extendRawArray[j+colorOffsets[green2Code].x][i+colorOffsets[green2Code].y];
                green1[j+off][i]=temp;
                green1[j+off][i+1]=temp;
            }
        cout<<"Nearest Neighbor demosaicing applied"<<endl;
        blocked=false;
    }
}
void RAWImageOld::bilinearDemosaic()
{
    if(!blocked)
    {
        blocked=true;
        //bilinear demosaic
        for(int j =0; j <  height; j++)
            for(int i = 0; i < width; i++)
            {
                switch(bayerFind(j, i))
                {
                case redCode:
                    //red at red
                    red[j][i]= extendRawArray[j][i];
                    //green at red
                    green1[j][i]=(extendRawArray[j+1][i]+extendRawArray[j][i+1]+extendRawArray[j-1][i]+extendRawArray[j][i-1])>>2;
                    //blue at red
                    blue[j][i]=(extendRawArray[j+1][i+1]+extendRawArray[j-1][i+1]+extendRawArray[j+1][i-1]+extendRawArray[j-1][i-1])>>2;
                    break;
                case green1Code:
                    //red at green1
                    red[j][i]=(extendRawArray[j][i+1]+extendRawArray[j][i-1])>>1;
                    //green1 at green1
                    green1[j][i]=extendRawArray[j][i];
                    //blue at green1
                    blue[j][i]=(extendRawArray[j+1][i]+extendRawArray[j-1][i])>>1;
                    break;
                case blueCode:
                    //red at blue
                    red[j][i]=(extendRawArray[j+1][i+1]+extendRawArray[j-1][i+1]+extendRawArray[j+1][i-1]+extendRawArray[j-1][i-1])>>2;
                    //green at blue
                    green1[j][i]=(extendRawArray[j+1][i]+extendRawArray[j][i+1]+extendRawArray[j-1][i]+extendRawArray[j][i-1])>>2;
                    //blue at blue
                    blue[j][i]=extendRawArray[j][i];
                    break;
                case green2Code:
                    //red at green2
                    red[j][i]=(extendRawArray[j+1][i]+extendRawArray[j-1][i])>>1;
                    //green1 at green2
                    green1[j][i]=extendRawArray[j][i];
                    //blue at green2
                    blue[j][i]=(extendRawArray[j][i+1]+extendRawArray[j][i-1])>>1;
                    break;
                }

            }
        cout<<"bilinear demosaicing applied"<<endl;
        blocked=false;
    }
}

void RAWImageOld::savePNG(std::string * file)
{
    if(!blocked)
    {
    blocked=true;
    spreadBits();
    PngReaderWriter writer;
    //array with RGB - 16 bits per color
    ushort* outputBuffer = new ushort[width*height*3];
    int l=0;
    //RGBG - Copy to Image
    for(int j = 0; j < height; j++)
    {
        for(int i = 0; i <  width; i++)
        {
            outputBuffer[l++]=red[j][i];
            outputBuffer[l++]=green1[j][i];
            outputBuffer[l++]=blue[j][i];
            //l+=3;
        }
    }
    cout<<"Buffer ready "<<file->c_str()<<endl;
    writer.setPrecision(precision);
    writer.saveNew((ushort *)outputBuffer,width, height, file);
    delete[] outputBuffer;
    cout<<"Image saved to "<<file->c_str()<<endl;
    blocked=false;
    }
}
