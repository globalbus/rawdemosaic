#include "Statistics.h"
#include <cmath>
#include "PngReaderWriter.h"
#include <algorithm>
#include <climits>
#include "Settings.h"
using namespace std;
Statistics::Statistics(string file, WorkingImage<arrayType, 3> * original, WorkingImage<arrayType, 3> * demosaiced)
{
    this->original = original;
    this->demosaiced = demosaiced;
    if(original!=nullptr){
    height=original->height;
    width=original->width;
    }
    this->file=file;
}
#ifdef _WIN32
#include <sstream>
template < class T >
string to_string( T t )
{
    stringstream ss;
    ss << t;
    return ss.str();
}
#endif
Statistics::Quality::Quality()
{

    csvFile.open("quality.csv", fstream::app);
    csvFile.seekp(0, std::ios_base::end);
    csvFile.setf(ios::fixed, ios::floatfield);
    if(csvFile.tellp()==0)
        csvFile<<firstColumn;
}

Statistics::Quality::~Quality()
{
    csvFile.close();
}
void Statistics::Quality::next(){
    csvFile<<endl;
    csvFile.flush();
}

Statistics::~Statistics()
{
    //dtor
}
void Statistics::doAll()
{
    if(original==nullptr||demosaiced==nullptr)
        return;
    getInstance().writeTo(Settings::fileNameOnly(&file));
    int temp;
    //
    long long d=USHRT_MAX;
    int threshold=d/100;
    int falseColors2=0;
    WorkingImage<arrayType,3> * originalCopy = original->extendWorkingImage(1);
    for(int j=0; j<height; j++)
        for(int i=0; i<width; i++){
        int falseColor=0;
        int falseColor2=0;
            for(int k=0; k<3; k++)
            {
                temp=abs(original->get(j,i,k)-demosaiced->get(j,i,k));
                if(temp>threshold)
                {
                    falseColor2++;
                    bool isfalseColor2=false;
                    for(int m=-1; m<=1; m++)
                        for(int n=-1; n<=1; n++)
                        if(abs(demosaiced->getR(j,i)-originalCopy->getR(j+m,i+n))<threshold
                        &&abs(demosaiced->getG1(j,i)-originalCopy->getG1(j+m,i+n))<threshold
                        &&abs(demosaiced->getB(j,i)-originalCopy->getB(j+m,i+n))<threshold){
                            isfalseColor2=true;
                            break;
                        }
                    if(isfalseColor2==false)
                        falseColor++;
                }
                //diff->checkedSet(j,i, k,(arrayType)temp);
                meanAbsoluteError+=temp;
                meanSquareError+=temp*temp;
            }
        if(falseColor>1)
            falseColors++;
        if(falseColor2>1)
            falseColors2++;
    }
    delete originalCopy;
    meanAbsoluteError/=3*height*width;
    meanSquareError/=3*height*width;

    peakToNoiseRatio=10*log10(d*d/meanSquareError);
    getInstance().writeTo(meanAbsoluteError);
    getInstance().writeTo(meanSquareError);
    getInstance().writeTo(peakToNoiseRatio);
    getInstance().writeTo((double)falseColors/(height*width));
    getInstance().writeTo((double)falseColors2/(height*width));
    cout<<"MAE "<<meanAbsoluteError<<endl;
    cout<<"MSE "<<meanSquareError<<endl;
    cout<<"PeakToNoise "<<peakToNoiseRatio<<endl;
    cout<<"FalseColors "<<(double)falseColors/(height*width)<<endl;
    cout<<"FalseColors with false-positives "<<(double)falseColors2/(height*width)<<endl;
    getInstance().next();
}
void Statistics::diff(string * file1, string * file2){
    PngReaderWriter pngWrapper;
    WorkingImage<arrayType, 3> * image1 = pngWrapper.read(file1);
    WorkingImage<arrayType, 3> * image2 = pngWrapper.read(file2);
    if(image1->height!=image2->height){
        return;
    }
    int height=image1->height;
    if(image1->width!=image2->width){
        return;
    }
    int width=image1->width;
    WorkingImage<arrayType, 3> * diff = new WorkingImage<arrayType, 3>(width, height);
    int temp;
    for(int j=0; j<height; j++)
        for(int i=0; i<width; i++){
            for(int k=0; k<3; k++)
            {
                temp=abs(image1->get(j,i,k)-image2->get(j,i,k));
                diff->checkedSet(j,i, k,(arrayType)temp);
            }
    }
    spreadBits(diff);
    pngWrapper.setLinearRGB(false);
    for(int k=0; k<3; k++)
    {
        ushort* outputBuffer = new ushort[(width)*(height)*1];
        string * file=new string(*file1+"diff"+to_string(k)+".png");
        int l=0;
        for(int j = 0; j < height; j++)
        {
            for(int i = 0; i < width; i++)
            {
                outputBuffer[l]=diff->get(j,i,k);
                l++;
            }
        }
        cout<<"Buffer ready "<<file->c_str()<<endl;
        pngWrapper.setPrecision(16);
        pngWrapper.saveNew(outputBuffer,width, height, file);

        delete[] outputBuffer;

        cout<<"Image saved to "<<file->c_str()<<endl;
        delete file;
    }
    delete diff;
    delete image1;
    delete image2;
}
void Statistics::spreadBits(WorkingImage<arrayType, 3> * data)
{
    arrayType * singleMemoryBlock=data->singleMemoryBlock;
    int height = data->height;
    int width = data->width;
    ushort maxValue = *max_element(singleMemoryBlock, singleMemoryBlock+width*height*3);
    //cout<<maxValue<<endl;
    ushort* linearToLinear = new ushort[maxValue+1];
    double toValue = (1<<16)-1;
    for (int i = 0; i < maxValue+1; i++)
    {
        linearToLinear[i] = round((toValue*i)/maxValue);
        //cout<<linearToLinear[i]<<endl;
    }
    for(int i=0; i<width*height*3; i++)
    {
        singleMemoryBlock[i]=linearToLinear[singleMemoryBlock[i]];
    }
    delete[] linearToLinear;
}
