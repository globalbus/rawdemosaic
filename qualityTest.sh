#!/bin/sh
FILESDIR=test-images/converted-png/
FILES=('IMG0001.png' 'IMG0005.png' 'IMG0007.png' 'IMG0008.png' 'IMG0019.png' 'IMG0023.png')
for index in ${!FILES[*]}
do
	printf -v FILES[$index] "%s%s" $FILESDIR${FILES[$index]}
	echo ${FILES[$index]}
done
bin/PseudoRawCLI ${FILES[*]} --output-dir=output $@
