#!/bin/sh
FILESDIR=test-images/RAWs/
FILES=('photo2010.09.05_21.30.19.99.dng' 'CRW_0858.DNG' 'SAM_0644.SRW' 'IMG_5372.CR2' 'IMGP5000.DNG')
for index in ${!FILES[*]}
do
	printf -v FILES[$index] "%s%s" $FILESDIR${FILES[$index]}
	echo ${FILES[$index]}
done
taskset 0x1 bin/RawCLI ${FILES[*]} --output-dir=output $@
